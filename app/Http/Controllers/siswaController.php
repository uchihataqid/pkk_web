<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Http;


class siswaController extends Controller
{
    public function getsiswa()
    {
        $client = new Client();
        $request = $client->get('http://project12rpl1.herokuapp.com/siswa');
        $response = $request->getBody()->getContents();

        $data = json_decode($response, true);

       return view('index',['data' => $data]);

    }
}
