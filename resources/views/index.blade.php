@extends('layouts.main')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col md-12">
                    <div class="panel">
						<div class="panel-heading">
                                <h3 class="panel-title">Data Siswa</h3>
                                <div class="right">
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-square"></i> <strong>Tambah data siswa</strong> </button>
                                </div>
						</div>
						<div class="panel-body">
							<table class="table table-hover">
								<thead>
									<tr>
                                        <th>id</th>
                                        <th>alamat</th>
                                        <th>nama</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($data as $data)
                                    <tr>

                                        <td>{{ $data['id'] }}</td>
                                        <td>{{ $data['alamat'] }}</td>
                                        <td>{{ $data['nama']}}</td>

                                    </tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
//GET DATA FROM API
        //$data = json_decode($response['employee_name'], true);
        //$response = Http::get('http://static.sekawanmedia.co.id/data.json')['data'];
       // $data = json_encode($response);
//SUCCESS
